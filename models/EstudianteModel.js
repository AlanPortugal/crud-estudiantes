    const mongoose = require('mongoose');
    const { Schema } = mongoose;

    const EstudianteSchema = new Schema({
        nombres: String,
        primerApellido: String,
        segundoApellido: String,
        numeroDocumento: Number
    });

    const Estudiante = mongoose.model('estudiante', EstudianteSchema);

    module.exports = Estudiante;
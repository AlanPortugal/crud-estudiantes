const verificarToken = (req, res, next) => {
    try {
        const { authorization } = req.headers;
        if (!authorization) {
            throw new Error('No autorizado.');
        }
        next();
    } catch (error) {
        res.status(401).json({
            finalizado: false,
            mensaje: error.message,
            datos: null
        });
    }

};

module.exports = {
    verificarToken
};
const { EstudianteRepository } = require('../repositories');

const findAll = async () => {
    const estudiantes = await EstudianteRepository.findAndCountAll();
    return estudiantes;
};

const guardar = async (datos) => {
    const estudiante = await EstudianteRepository.createOrUpdate(datos);
    return estudiante;
};

const eliminar = async (_id) => {
    const estudianteEliminado = await EstudianteRepository.deleteItem(_id);
    return estudianteEliminado;
};

module.exports = {
    findAll,
    guardar,
    eliminar
};
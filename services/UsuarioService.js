const { UsuarioRepository } = require('../repositories');

const login = async (usuario, contrasena) => {
    const existeUsuario = await UsuarioRepository.findOne({ usuario, contrasena });
    return existeUsuario;
};

const guardar = async (datos) => {
    const usuario = await UsuarioRepository.createOrUpdate(datos);
    return usuario;
};

module.exports = {
    login,
    guardar
};
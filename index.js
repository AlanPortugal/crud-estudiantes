const express = require('express');
const cors = require('cors');
const app = express();
const port = process.env.PORT || 3000;
const bodyParser = require('body-parser');
const { EstudianteRoute, AuthRoute, UsuarioRoute } = require('./routes');


const mongoose = require('mongoose');
const {db} = require('./config');
mongoose.connect(db.urlConexion, { useNewUrlParser: true, useUnifiedTopology: true });

app.use(cors());
app.use(bodyParser.json());
app.use(EstudianteRoute);
app.use(AuthRoute);
app.use(UsuarioRoute);

app.listen( port, () => {
    console.log(`===> FUNCIONANDO EN EL PUERTO: ${port}`);
});
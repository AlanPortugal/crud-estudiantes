const { UsuarioModel } = require('../models');
const Repository = require('./Repository');

module.exports = {
    findOne: params => Repository.findOne(UsuarioModel, params),
    createOrUpdate: datos => Repository.createOrUpdate(UsuarioModel, datos)
};
const { EstudianteModel } = require('../models');
const Repository = require('./Repository');

module.exports = {
    findAndCountAll: () => Repository.findAndCountAll(EstudianteModel),
    createOrUpdate: datos => Repository.createOrUpdate(EstudianteModel, datos),
    deleteItem: _id => Repository.deleteItem(EstudianteModel, _id)
};
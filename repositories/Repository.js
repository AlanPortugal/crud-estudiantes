const findAndCountAll = async (modelo) => {
    const count = await modelo.count();
    const rows = await modelo.find();
    return { count, rows }
};

const createOrUpdate = async (modelo, datos) => {
    if (datos._id) {
        const _existeRegistros = await modelo.findById(datos._id);
        if (_existeRegistros) {
            const _registroActualizado = await modelo.update({ _id: datos._id }, { $set: datos });
            return _registroActualizado;
        } else {
            const _registroCreado = await modelo.create(datos);
            return _registroCreado;
        }
    }

    const _registroCreado = await modelo.create(datos);
    return _registroCreado;
};

const deleteItem = (modelo, _id) => {
    return modelo.findByIdAndRemove(_id);
};

const findOne = async (modelo, params) => {
    const respuesta = await modelo.findOne(params);
    if (!respuesta) {
        return null;
    }
    return respuesta;
};

module.exports = {
    findAndCountAll,
    createOrUpdate,
    deleteItem,
    findOne
};
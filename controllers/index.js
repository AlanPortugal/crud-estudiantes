module.exports = {
    EstudianteController: require('./EstudianteController'),
    AuthController: require('./AuthController'),
    UsuarioController: require('./UsuarioController')
};
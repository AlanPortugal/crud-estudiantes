const { EstudianteService } = require('../services');
const { mensajeExito, mensajeError } = require('./Respuesta');

const listarEstudiantes = async (req, res) => {
    try {
        const listaEstudiantes = await EstudianteService.findAll();
        return mensajeExito(res, 200, 'Listado de Estudiantes', listaEstudiantes);
    } catch (error) {
        return mensajeError(res, 400, 'Error en el listado de estudiantes.', error.message);
    }
};

const registrarEstudiante = async (req, res) => {
    try {
        const datos = req.body;
        const estudianteCreado = await EstudianteService.guardar(datos);
        return mensajeExito(res, 201, 'Estudiante Resgistrado Correctamente.', estudianteCreado);
    } catch (error) {
        return mensajeError(res, 400, 'Estudiante NO Registrado.', error.message);
    }
};

const modificarEstudiante = async (req, res) => {
    try {
        const { _id } = req.params;
        const datos = req.body;
        datos._id = _id;
        const estudianteModificado = await EstudianteService.guardar(datos);
        return mensajeExito(res, 201, 'Estudiante Modificado Correctamente.', estudianteModificado);
    } catch (error) {
        return mensajeError(res, 400, 'Estudiante NO Modificado.', error.message);
    }
};

const eliminarEstudiante = async (req, res) => {
    const { _id }= req.params;
    try {
        const estudianteEliminado = await EstudianteService.eliminar(_id);
        return mensajeExito(res, 200, 'Estudiante Eliminado Correctamente', estudianteEliminado);
    } catch (error) {
        return mensajeError(res, 400, `Persona NO Eliminada: ${_id}`, error.message);
    }
};

module.exports = {
    listarEstudiantes,
    registrarEstudiante,
    modificarEstudiante,
    eliminarEstudiante
};
const { app } = require('../config');
const { mensajeExito, mensajeError } = require('./Respuesta');
const { UsuarioService } = require('../services');
const jwt = require('jsonwebtoken');

const login = async (req, res) => {
    try {
        const { usuario, contrasena } = req.body;
        const respuesta = await UsuarioService.login(usuario, contrasena);
        if (!respuesta) {
            throw new Error('Error con sus creddenciales de acceso');
        }
        delete respuesta.contrasena;
        const token = jwt.sign({ ...respuesta, exp: app.auth.exp }, app.auth.secret);
        return mensajeExito(res, 200, 'Ingreso al sistema correctamente.', { ...respuesta,token });
    } catch (error) {
        return mensajeError(res, 400, 'Error al ingresar al sistema.', error.message);
    }
};

module.exports = {
    login
};
const mensajeExito = (res, code, mensaje, datos) => {
    return res.status(code).json({
        finalizado: true,
        mensaje,
        datos
    })
};

const mensajeError = (res, code, mensaje, datos) => {
    return res.status(code).json({
        finalizado: false,
        mensaje,
        datos
    })
};

module.exports = {
    mensajeExito,
    mensajeError
};
const { UsuarioService } = require('../services');
const { mensajeExito, mensajeError } = require('./Respuesta');

const registrarUsuario = async (req, res) => {
    try {
        const datos = req.body;
        const usuarioCreado = await UsuarioService.guardar(datos);
        return mensajeExito(res, 201, 'Usuario Resgistrado Correctamente.', usuarioCreado);
    } catch (error) {
        return mensajeError(res, 400, 'Usuario NO Registrado.', error.message);
    }
};

const modificarUsuario = async (req, res) => {
    try {
        const { _id } = req.params;
        const datos = req.body;
        datos._id = _id;
        const usuarioModificado = await UsuarioService.guardar(datos);
        return mensajeExito(res, 201, 'Usuario Modificado Correctamente.', usuarioModificado);
    } catch (error) {
        return mensajeError(res, 400, 'Usuario NO Modificado.', error.message);
    }
};

const login = async (req, res) => {
    try {
        const { usuario, contrasena } = req.params;
        const respuesta = await UsuarioService.login(usuario, contrasena);
        return mensajeExito(res, 201, 'Usuario Identificado Correctamente.', respuesta);
    } catch (error) {
        return mensajeError(res, 400, 'Usuario NO Identificado.', error.message);
    }
};

module.exports = {
    registrarUsuario,
    modificarUsuario,
    login
};
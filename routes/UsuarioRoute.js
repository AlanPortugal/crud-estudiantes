const express = require('express');
const router = express.Router();
const { UsuarioController } = require('../controllers');

// Registrar Usuario
router.post('/usuarios', UsuarioController.registrarUsuario);
// Modificar Usuario
router.put('/usuarios/:_id', UsuarioController.modificarUsuario);

module.exports = router;
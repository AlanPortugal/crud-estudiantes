const express = require('express');
const router = express.Router();
const { EstudianteController } = require('../controllers');
const { AuthMiddleware, PermisosMiddleware } = require('../middlewares');

// Listar Estudiantes
router.get('/estudiantes', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos('usuarios:listar'), EstudianteController.listarEstudiantes);
// Registrar Estudiante
router.post('/estudiantes', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos('usuarios:crear'), EstudianteController.registrarEstudiante);
// Modificar Estudiante
router.put('/estudiantes/:_id', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos('usuarios:editar'), EstudianteController.modificarEstudiante);
// Eliminar Estudiante
router.delete('/estudiantes/:_id', AuthMiddleware.verificarToken, PermisosMiddleware.verificarPermisos('usuarios:eliminar'), EstudianteController.eliminarEstudiante);


module.exports = router;